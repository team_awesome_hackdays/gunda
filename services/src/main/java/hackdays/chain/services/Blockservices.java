package hackdays.chain.services;

import hackdays.gunda.actions.TransportObject;

public interface Blockservices {
    TransportObject writeToChain(TransportObject data);
    TransportObject readFromChain(TransportObject data);
}