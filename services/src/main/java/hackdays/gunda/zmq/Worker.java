package hackdays.gunda.zmq;

import hackdays.gunda.actions.Action;
import org.zeromq.ZMQ;

import java.nio.charset.Charset;

public class Worker extends Thread
{
    private ZMQ.Context context;
    private Action action;

    public Worker (ZMQ.Context context, Action action)
    {
        this.context = context;
        this.action = action;
    }


    @Override
    public void run() {
        ZMQ.Socket socket = context.socket(ZMQ.REP);
        socket.connect ("inproc://workers");

        StringBuilder retval = new StringBuilder();

        while (true) {

            //  Wait for next request from client (C string)
            String request = socket.recvStr(Charset.forName("UTF-8"));
            System.out.println ( Thread.currentThread().getName() + " Received request: [" + request + "]");

            socket.send(action.action(request));

        }
    }
}
