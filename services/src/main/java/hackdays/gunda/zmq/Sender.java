package hackdays.gunda.zmq;

import org.zeromq.ZMQ;

import java.nio.charset.Charset;

public class Sender {
    public String send(String message, ZMQ.Context context, String adress, String port) {
        ZMQ.Socket requester = context.socket(ZMQ.REQ);
        requester.setLinger(2400);
        requester.setReceiveTimeOut(60000);
        requester.connect("tcp://" + adress + ":" + port);
        requester.send(message);
        return requester.recvStr(Charset.forName("UTF-8"));
    }
}
