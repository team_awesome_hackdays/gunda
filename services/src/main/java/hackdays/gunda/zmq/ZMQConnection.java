package hackdays.gunda.zmq;

import hackdays.gunda.actions.Action;
import org.zeromq.ZMQ;

import java.util.HashMap;
import java.util.Map;

public class ZMQConnection {

    public void connecting(int port, Action action) {

        ZMQ.Context context = ZMQ.context(1);

        ZMQ.Socket clients = context.socket(ZMQ.ROUTER);
        clients.bind("tcp://*:"+port);

        ZMQ.Socket workers = context.socket(ZMQ.DEALER);
        workers.bind("inproc://workers");

        Map<Integer, Thread> threadList = new HashMap<>();

        for (int thread_nbr = 0; thread_nbr < 5; thread_nbr++) {

            Thread worker = new Worker(context, action);
            worker.start();
            threadList.put(thread_nbr, worker);

        }
        //  Connect work threads to client threads via a queue
        ZMQ.proxy(clients, workers, null);

        //  We never get here but clean up anyhow
        clients.close();
        workers.close();
        context.term();
    }
}