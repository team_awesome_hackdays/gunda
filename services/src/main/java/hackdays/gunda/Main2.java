package hackdays.gunda;

import hackdays.gunda.zmq.Sender;
import org.zeromq.ZMQ;

public class Main2 {
    public static void main(String[] args) {
        Sender sender = new Sender();
        ZMQ.Context context = ZMQ.context(1);

        System.out.println(sender.send("Hallo", context, "192.168.116.104", "5555"));
    }
}
