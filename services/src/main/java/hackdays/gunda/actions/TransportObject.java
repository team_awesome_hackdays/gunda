package hackdays.gunda.actions;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;
import org.bson.Document;

import java.util.Map;


public class TransportObject extends BasicDBObject {
    public enum TransportObjectKeys  {
        ACTION, DATA, _id, BLOCKCHAINADRESS, STATUS, transactionid
    }
    public enum TransportObjectStatus  {
        OK, NOK
    }

    private String action;
    private String data;
    private String status;

    public TransportObject(String action, String data) {
        setData(data);
        setAction(action);
    }

    public TransportObject(Document map) {
        super(map);
    }

    public TransportObject(BasicDBObject o) {
        for(String key: o.keySet()) {
            put(key, o.get(key));
        }
    }

    public TransportObject(String json){
        super((BasicDBObject)JSON.parse(json));
    }

    public void setAction(String action) {
        this.action = action;
        put(TransportObjectKeys.ACTION.name(), action);
    }

    public void setData(String data) {
        this.data = data;
        put(TransportObjectKeys.DATA.name(), data);
    }

    public void setStatus(String status) {
        this.status = status;
        put(TransportObjectKeys.STATUS.name(), status);
    }

    public String getData() {
        return getString(TransportObjectKeys.DATA.name());
    }

    public String getStatus() {
        return getString(TransportObjectKeys.STATUS.name());
    }

    public String getAction() {
        return getString(TransportObjectKeys.ACTION.name());
    }

    public BasicDBObject getDataObject() {
        return (BasicDBObject) JSON.parse(getString(TransportObjectKeys.DATA.name()));
    }

}
