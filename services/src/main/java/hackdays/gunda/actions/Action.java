package hackdays.gunda.actions;

public interface Action {
    public enum ActionTypes {
        SAVE, READ
    }

    public String action(String request);
}
