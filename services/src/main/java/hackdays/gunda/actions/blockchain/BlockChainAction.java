package hackdays.gunda.actions.blockchain;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;
import hackdays.chain.services.Blockservices;
import hackdays.gunda.actions.Action;
import hackdays.gunda.actions.TransportObject;
import hackdays.gunda.zmq.Sender;
import org.zeromq.ZMQ;

public class BlockChainAction implements Action {
    Blockservices blockservices;

    BlockChainAction(Blockservices blockservices){
        this.blockservices = blockservices;
    }

    public String action(String request){
        BasicDBObject json = (BasicDBObject) JSON.parse(request);
        ZMQ.Context context = ZMQ.context(1);

        TransportObject transportObject = new TransportObject(json);
        String action = transportObject.getAction();
        if(ActionTypes.SAVE.name().equals(action)) {
            new Sender().send(transportObject.toString(), context, "localhost", "5557");
            // transportObject = blockservices.writeToChain(transportObject);
        } else {
            new Sender().send(transportObject.toString(), context, "localhost", "5557");
            // transportObject = blockservices.readFromChain(transportObject);
        }
        return transportObject.toString();
    }

}
