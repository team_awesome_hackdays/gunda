package hackdays.gunda.actions.email;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Xml450Writer {
    private final String fileName;
    private final String tangle;
    private List<Map.Entry<String, String>> leistungen = new ArrayList<>();

    Xml450Writer(String fileName, String tangle) {
        this.fileName = fileName;
        this.tangle = tangle;
    }

    public void addLeistung(String anzahl, String code) {
        Map.Entry entry = new AbstractMap.SimpleEntry(code, anzahl);
        leistungen.add(entry);
    }

    public void write() {
        File out = new File(fileName);
        try (FileWriter writer = new FileWriter(out)) {
            writer.write(getXml());
            writer.flush();
        } catch (IOException e) {
            System.err.print(e);
        }
    }

    private String getXml() {
        String l = "";
        int count = 0;
        for (Map.Entry<String, String> entry : leistungen) {
            l += XML_LEISTUNG.replace("{{QUANTITY}}", entry.getKey())
                    .replace("{{UNIT}}", "1337")
                    .replace("{{CODE}}", entry.getValue())
                    .replace("{{NR}}", String.valueOf(count++))
                    .replace("{{SECTION_CODE}}", entry.getValue())
                    .replace("{{BEGIN}}", "2017-12-01T08:09:10")
                    .replace("{{END}}", "2017-12-02T15:16:17")
                    .replace("{{NAME}}", "Name von " + entry.getValue());
        }
        return XML_TEMPLATE.replace("{{LEISTUNGEN}}", l);
    }

    private final String XML_LEISTUNG = "        <invoice:service record_id=\"{{NR}}\" tariff_type=\"{{TYPE}}\" code=\"{{CODE}}\" session=\"1\" quantity=\"{{QUANTITY}}\"" +
            " date_begin=\"{{BEGIN}}\" date_end=\"{{END}}\" provider_id=\"7634567890111\" responsible_id=\"7634567890333\" unit=\"{{UNIT}}\" unit_factor=\"{{UNIT_FACTOR}}\"" +
            " external_factor=\"{{EXT_FACTOR}}\" amount=\"{{AMOUNT}}\" service_attributes=\"0\" obligation=\"1\" name=\"{{NAME}}\" remark=\"{{REMARK}}\" section_code=\"{{SECTION_CODE}}\"/>\n";

    private final String XML_TEMPLATE = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
            "<invoice:request xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xenc=\"http://www.w3.org/2001/04/xmlenc#\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:invoice=\"http://www.forum-datenaustausch.ch/invoice\" xmlns=\"http://www.forum-datenaustausch.ch/invoice\" xsi:schemaLocation=\"http://www.forum-datenaustausch.ch/invoice generalInvoiceRequest_450.xsd\" language=\"de\" modus=\"production\" validation_status=\"0\">\n" +
            "  <invoice:processing>\n" +
            "    <invoice:transport from=\"7601001302112\" to=\"7634567890000\">\n" +
            "      <invoice:via via=\"2000012345678\" sequence_id=\"1\"/>\n" +
            "    </invoice:transport>\n" +
            "  </invoice:processing>\n" +
            "  <invoice:payload type=\"invoice\" copy=\"0\" storno=\"0\">\n" +
            "    <invoice:invoice request_timestamp=\"1511421400\" request_date=\"2017-11-24T00:00:00\" request_id=\"212_01:001\"/>\n" +
            "    <invoice:body role=\"hospital\" place=\"hospital\">\n" +
            "      <invoice:prolog>\n" +
            "        <invoice:package name=\"GeneralInvoiceRequestTest\" copyright=\"suva 2000-17\" version=\"100015\"/>\n" +
            "        <invoice:generator name=\"GeneralInvoiceRequestManager 4.50.003\" copyright=\"suva 2000-17\" version=\"450\"/>\n" +
            "      </invoice:prolog>\n" +
            "      <invoice:remark>Lorem ipsum per nostra mi fune torectum\n" +
            "\n" +
            " mikonstra.diloru si limus mer fin per od per nostra mi fune torectum mi konstradiloru si limus mer fin itorectum mi konstradiloruko.</invoice:remark>\n" +
            "      <invoice:tiers_payant>\n" +
            "        <invoice:biller ean_party=\"2011234567890\" zsr=\"H121111\" uid_number=\"CHE108791452\">\n" +
            "          <invoice:company>\n" +
            "            <invoice:companyname>Biller AG</invoice:companyname>\n" +
            "            <invoice:department>Abteilung Inkasso</invoice:department>\n" +
            "            <invoice:postal>\n" +
            "              <invoice:street>Billerweg 128</invoice:street>\n" +
            "              <invoice:zip>4414</invoice:zip>\n" +
            "              <invoice:city>Frenkendorf</invoice:city>\n" +
            "            </invoice:postal>\n" +
            "            <invoice:telecom>\n" +
            "              <invoice:phone>061 956 99 00</invoice:phone>\n" +
            "              <invoice:fax>061 956 99 10</invoice:fax>\n" +
            "            </invoice:telecom>\n" +
            "            <invoice:online>\n" +
            "              <invoice:email>info@biller.ch</invoice:email>\n" +
            "            </invoice:online>\n" +
            "          </invoice:company>\n" +
            "        </invoice:biller>\n" +
            "        <invoice:debitor ean_party=\"7634567890000\">\n" +
            "          <invoice:company>\n" +
            "            <invoice:companyname>Krankenkasse AG</invoice:companyname>\n" +
            "            <invoice:department>Sektion Basel</invoice:department>\n" +
            "            <invoice:postal>\n" +
            "              <invoice:street>Kassengraben 222</invoice:street>\n" +
            "              <invoice:zip>4000</invoice:zip>\n" +
            "              <invoice:city>Basel</invoice:city>\n" +
            "            </invoice:postal>\n" +
            "          </invoice:company>\n" +
            "        </invoice:debitor>\n" +
            "        <invoice:provider ean_party=\"7634567890111\" zsr=\"P123456\">\n" +
            "          <invoice:company>\n" +
            "            <invoice:companyname>Klinik für Psychiatrie</invoice:companyname>\n" +
            "            <invoice:postal>\n" +
            "              <invoice:street>Spitalgasse 17b5</invoice:street>\n" +
            "              <invoice:zip statecode=\"BS\">4000</invoice:zip>\n" +
            "              <invoice:city>Basel</invoice:city>\n" +
            "            </invoice:postal>\n" +
            "            <invoice:telecom>\n" +
            "              <invoice:phone>061 956 99 00</invoice:phone>\n" +
            "              <invoice:fax>061 956 99 10</invoice:fax>\n" +
            "            </invoice:telecom>\n" +
            "          </invoice:company>\n" +
            "        </invoice:provider>\n" +
            "        <invoice:insurance ean_party=\"7634567890000\">\n" +
            "          <invoice:company>\n" +
            "            <invoice:companyname>Krankenkasse AG</invoice:companyname>\n" +
            "            <invoice:department>Sektion Basel</invoice:department>\n" +
            "            <invoice:postal>\n" +
            "              <invoice:street>Kassengraben 222</invoice:street>\n" +
            "              <invoice:zip>4000</invoice:zip>\n" +
            "              <invoice:city>Basel</invoice:city>\n" +
            "            </invoice:postal>\n" +
            "          </invoice:company>\n" +
            "        </invoice:insurance>\n" +
            "        <invoice:patient gender=\"male\" birthdate=\"1964-05-14T00:00:00\" ssn=\"12345678901\">\n" +
            "          <invoice:person salutation=\"Herr\">\n" +
            "            <invoice:familyname>Muster</invoice:familyname>\n" +
            "            <invoice:givenname>Peter</invoice:givenname>\n" +
            "            <invoice:postal>\n" +
            "              <invoice:street>Musterstrasse 5</invoice:street>\n" +
            "              <invoice:zip>7304</invoice:zip>\n" +
            "              <invoice:city>Maienfeld</invoice:city>\n" +
            "            </invoice:postal>\n" +
            "          </invoice:person>\n" +
            "          <invoice:card card_id=\"12345678901234567890\" expiry_date=\"2018-03-03T00:00:00\"/>\n" +
            "        </invoice:patient>\n" +
            "        <invoice:insured gender=\"male\" birthdate=\"1981-08-03T00:00:00\" ssn=\"7562632552237\">\n" +
            "          <invoice:person salutation=\"Herr\">\n" +
            "            <invoice:familyname>Muster-Vorlage</invoice:familyname>\n" +
            "            <invoice:givenname>Hans</invoice:givenname>\n" +
            "            <invoice:postal>\n" +
            "              <invoice:street>Musterstrasse 5</invoice:street>\n" +
            "              <invoice:zip>5304</invoice:zip>\n" +
            "              <invoice:city>Maienfeld</invoice:city>\n" +
            "            </invoice:postal>\n" +
            "          </invoice:person>\n" +
            "          <invoice:card card_id=\"23456789012345678901\" expiry_date=\"2018-02-08T00:00:00\"/>\n" +
            "        </invoice:insured>\n" +
            "        <invoice:guarantor>\n" +
            "          <invoice:person salutation=\"Herr\">\n" +
            "            <invoice:familyname>Muster-Vorlage</invoice:familyname>\n" +
            "            <invoice:givenname>Hans</invoice:givenname>\n" +
            "            <invoice:postal>\n" +
            "              <invoice:street>Musterstrasse 5</invoice:street>\n" +
            "              <invoice:zip>5304</invoice:zip>\n" +
            "              <invoice:city>Maienfeld</invoice:city>\n" +
            "            </invoice:postal>\n" +
            "          </invoice:person>\n" +
            "        </invoice:guarantor>\n" +
            "        <invoice:balance currency=\"CHF\" amount=\"4821.60\" amount_obligations=\"4821.60\" amount_due=\"4821.60\">\n" +
            "          <invoice:vat vat=\"0.00\" vat_number=\"CHE108791452\">\n" +
            "            <invoice:vat_rate vat=\"0.00\" vat_rate=\"0\" amount=\"4821.60\"/>\n" +
            "          </invoice:vat>\n" +
            "        </invoice:balance>\n" +
            "      </invoice:tiers_payant>\n" +
            "      <invoice:esrQR iban=\"LI1008800000020176306\" type=\"esrQR\" reference_number=\"RF18539007547034\" customer_note=\"customer note\">\n" +
            "        <invoice:bank>\n" +
            "          <invoice:company>\n" +
            "            <invoice:companyname>Bank AG</invoice:companyname>\n" +
            "            <invoice:postal>\n" +
            "              <invoice:zip countrycode=\"CH\">4002</invoice:zip>\n" +
            "              <invoice:city>Basel</invoice:city>\n" +
            "            </invoice:postal>\n" +
            "          </invoice:company>\n" +
            "        </invoice:bank>\n" +
            "        <invoice:creditor>\n" +
            "          <invoice:company>\n" +
            "            <invoice:companyname>Biller AG</invoice:companyname>\n" +
            "            <invoice:postal>\n" +
            "              <invoice:street>Billerweg 128</invoice:street>\n" +
            "              <invoice:zip countrycode=\"CH\">4414</invoice:zip>\n" +
            "              <invoice:city>Frenkendorf</invoice:city>\n" +
            "            </invoice:postal>\n" +
            "          </invoice:company>\n" +
            "        </invoice:creditor>\n" +
            "      </invoice:esrQR>\n" +
            "      <invoice:kvg case_id=\"123456-6789\" case_date=\"2017-11-23T00:00:00\" insured_id=\"123.45.678-012\"/>\n" +
            "      <invoice:treatment date_begin=\"2017-10-27T22:40:38\" date_end=\"2017-11-23T08:16:38\" canton=\"BS\" reason=\"disease\" apid=\"tarPSYID_1456\" acid=\"tarPSY005.4\">\n" +
            "        <invoice:diagnosis type=\"ICD\" code=\"J18.9\">Sonstige Schizophrenie</invoice:diagnosis>\n" +
            "        <invoice:xtra_hospital>\n" +
            "          <invoice:stationary section_major=\"M00\" hospitalization_type=\"regular\" hospitalization_mode=\"noncantonal_indicated\" class=\"general\" treatment_days=\"P24D\" hospitalization_date=\"2017-10-27T22:40:38\" has_expense_loading=\"0\">\n" +
            "            <invoice:admission_type number=\"0\" name=\"normal\"/>\n" +
            "            <invoice:discharge_type number=\"0\" name=\"normal\"/>\n" +
            "            <invoice:provider_type number=\"2\" name=\"Psychiatrische Klinik\"/>\n" +
            "            <invoice:bfs_residence_before_admission code=\"1\" name=\"Zuhause\"/>\n" +
            "            <invoice:bfs_admission_type code=\"3\" name=\"angemeldet, geplant\"/>\n" +
            "            <invoice:bfs_decision_for_discharge code=\"1\" name=\"auf Initiative des Behandelnden\"/>\n" +
            "            <invoice:bfs_residence_after_discharge code=\"1\" name=\"Zuhause\"/>\n" +
            "          </invoice:stationary>\n" +
            "        </invoice:xtra_hospital>\n" +
            "      </invoice:treatment>\n" +
            "      <invoice:services>\n" +
            "{{LEISTUNGEN}}" +
            "      </invoice:services>\n" +
            "    </invoice:body>\n" +
            "  </invoice:payload>\n" +
            "</invoice:request>\n";
}
