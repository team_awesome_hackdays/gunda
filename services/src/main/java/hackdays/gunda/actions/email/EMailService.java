package hackdays.gunda.actions.email;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;
import hackdays.gunda.DataService;
import hackdays.gunda.actions.TransportObject;
import org.bson.Document;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class EMailService {

    public static void sendEmail(TransportObject doc){
        // Recipient's email ID needs to be mentioned.
        String to = "fabian.brunner@adcubum.com";

        // Sender's email ID needs to be mentioned
        String from = "team_awesome@adcubum.com";

        // Assuming you are sending email from localhost
        String host = "mailservicein.swisscom.com";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty("mail.smtp.host", host);

        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties);

        String subject = "new Entries in your Chain!";

        BasicDBObject data = (BasicDBObject)JSON.parse(doc.getData());
        BasicDBList transactionHashes = (BasicDBList) data.get("transactionHashes");
        List<String> list = new ArrayList<>();
        transactionHashes.forEach(c -> list.add(c.toString()));

        String text = "Hallo René :o)\n\nDas ist der Hash welcher in der IOTA nachgeschaut werden kann:\n" + list.stream().collect(Collectors.joining(", "));
        text += "\n Hier kannst du schauen was da drin steht:\n\thttp://iota.team_awesome.ranta.ch/#/search/any/" + list.get(0);
        text += "\n\n";
        text += "Der Versicherung kannst du diesen Link senden, da verbirgt sich das XML-451:\n\t";
        text += "\n\thttps://middleware.team_awesome.ranta.ch/Leistungen_" + list.get(0) + ".xml";

        String fileName = "/var/www/html/Leistungen_" + list.get(0) + ".xml";
        Xml450Writer xml = new Xml450Writer(fileName, list.get(0));

        List<BasicDBObject> leistungen = (List<BasicDBObject>)data.get("leistungen");

        for (BasicDBObject x : leistungen) {
            xml.addLeistung(x.get("anzahl").toString(), x.get("code").toString());
        }
        xml.write();


        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject(subject);

            // Now set the actual message
            message.setText(text);

            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }

}
