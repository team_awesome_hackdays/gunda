package hackdays.gunda.actions.database;

import hackdays.gunda.actions.Action;
import hackdays.gunda.actions.TransportObject;
import hackdays.gunda.actions.email.EMailService;
import hackdays.gunda.zmq.Sender;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.zeromq.ZMQ;

public class DataBaseAction  implements Action{
    @Override
    public String action(String request) {
        System.out.println("Got Request: " + request);
        TransportObject requestTransport = new TransportObject(request);
        if(ActionTypes.SAVE.name().equals(requestTransport.getAction())) {
            requestTransport = Saver.getInstance().saveDocument(requestTransport, "Leistungen");
            ZMQ.Context context = ZMQ.context(1);

            // Blockchain aufrufen
            String response = new Sender().send(requestTransport.toString(), context, "localhost", "5557");
            System.out.println("Got Response: " + response);
            TransportObject transportObject = new TransportObject(response);
            transportObject = requestTransport = Saver.getInstance().saveDocument(transportObject, "Leistungen");

            // EMail senden
            EMailService.sendEmail(requestTransport);
            requestTransport.setStatus(TransportObject.TransportObjectStatus.OK.name());
            return requestTransport.toString();
        } else{
            ObjectId transactionId = (ObjectId)requestTransport.get(TransportObject.TransportObjectKeys.transactionid);
            Document doc = new Search().searchById(transactionId, "Leistungen");
            return doc.toString();
        }
    }
}
