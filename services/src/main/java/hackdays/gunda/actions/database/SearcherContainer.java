package hackdays.gunda.actions.database;

import com.mongodb.client.FindIterable;
import org.bson.Document;

public class SearcherContainer {
    private FindIterable<Document> documents;
    private long count;

    public FindIterable<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(FindIterable<Document> documents) {
        this.documents = documents;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}