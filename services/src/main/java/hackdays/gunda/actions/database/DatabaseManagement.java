package hackdays.gunda.actions.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ReadPreference;
import com.mongodb.client.MongoDatabase;

/**
 * Created by Alexander Christ on 11.07.17.
 */
public class DatabaseManagement {

    private static final class InstanceHolder {
        static DatabaseManagement INSTANCE = new DatabaseManagement();
    }


    private DatabaseManagement() {
        connect();
    }

    public static DatabaseManagement getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private MongoClient m;
    private MongoDatabase db;

    protected void connect() {

        MongoClientOptions settings = MongoClientOptions.builder().readPreference(ReadPreference.primaryPreferred())
                .codecRegistry(MongoClient.getDefaultCodecRegistry()).build();

            m = new MongoClient("localhost", settings);
            db = m.getDatabase("teamawesome");
    }

    public MongoClient getMongoClient() {
        return m;
    }

    public MongoDatabase getDb() {
        return db;
    }
}
