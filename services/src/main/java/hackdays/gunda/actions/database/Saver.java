package hackdays.gunda.actions.database;


import com.mongodb.BasicDBObject;
import com.mongodb.client.result.UpdateResult;
import hackdays.gunda.actions.Action;
import hackdays.gunda.actions.TransportObject;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 * Created by Alexander Christ on 16.10.17.
 */
public class Saver {
    private final DatabaseManagement vtdb;


    private static final class InstanceHolder {
        static Saver INSTANCE = new Saver();
    }

    public static Saver getInstance() {
        return Saver.InstanceHolder.INSTANCE;
    }

    public Saver() {
        vtdb = DatabaseManagement.getInstance();

    }


    public TransportObject saveDocument(TransportObject doc, String collection)  {
        Document docIn = new Document(doc.getDataObject());

        if(docIn.get(TransportObject.TransportObjectKeys._id.name()) != null) {
            docIn = updateDocument(docIn, collection);
        } else {
            vtdb.getDb().getCollection(collection).insertOne(docIn);
            if(docIn.getObjectId(TransportObject.TransportObjectKeys._id.name()) == null) {
             //Save fail
            }
        }

        doc = new TransportObject(Action.ActionTypes.SAVE.name(), docIn.toJson());
        return doc;
    }

    private Document updateDocument(Document doc, String collection){

        final UpdateResult updateResult;

        String id = null;
        try {
            id = doc.getString(TransportObject.TransportObjectKeys._id.name());
        }catch(Exception e) {
            id = doc.getObjectId(TransportObject.TransportObjectKeys._id.name()).toString();
        }

        doc.remove(TransportObject.TransportObjectKeys._id.name());
        updateResult = vtdb.getDb().getCollection(collection).replaceOne(new BasicDBObject(TransportObject.TransportObjectKeys._id.name(), new ObjectId(id)), doc);

        doc.append(TransportObject.TransportObjectKeys._id.name(), id);
        if(updateResult.getModifiedCount() == 0) {
            //Fail update
        }
        return doc;
    }
}
