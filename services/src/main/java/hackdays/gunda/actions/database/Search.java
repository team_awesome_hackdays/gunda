package hackdays.gunda.actions.database;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.util.JSON;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Search {
    private final DatabaseManagement vtdb;

    public Search() {
        vtdb = DatabaseManagement.getInstance();

    }

    /**
     *
     * @param
     * @return
     *
     *
     */
    public Document searchById(ObjectId id, String collection) {
        return vtdb.getDb().getCollection(collection).find(Filters.eq(id)).first();
    }

    public List<BasicDBObject> searchObjectsByQuery(String query, int skip, int limit) {
        BasicDBObject queryObject = (BasicDBObject) JSON.parse(query);

        BasicDBObject search = (BasicDBObject) queryObject.get("data");
        List<BasicDBObject> retVal = new ArrayList<>();
        SearcherContainer container = getDocuments(query, skip, limit);
        for(Document doc: container.getDocuments()) {
            retVal.add(new BasicDBObject(doc));
        }
        return retVal;
    }

    private SearcherContainer getDocuments(String query, int skip, int limit) {
        SearcherContainer retVal = new SearcherContainer();
        BasicDBObject queryObject = (BasicDBObject) JSON.parse(query);
        BasicDBObject search = (BasicDBObject) queryObject.get("data");
        List<BasicDBObject> conditionList = new ArrayList<>();
        for(String field: search.keySet()) {
            conditionList.add(buildAndCondition(field, search.getString(field)));
        }

        BasicDBObject searchBson = new BasicDBObject("$" + search.getString("$and"), conditionList);
        FindIterable<Document> documents;
        long count = vtdb.getDb().getCollection("leistungen").count(searchBson);


        retVal.setCount(count);

        documents = vtdb.getDb().getCollection(search.getString("leistungen")).find(new BasicDBObject("$" + search.getString("and"), conditionList));
        retVal.setDocuments(documents);
        return retVal;
    }

    private BasicDBObject buildAndCondition(String field, String value) {
        return new BasicDBObject(new BasicDBObject("$and", Arrays.asList(new BasicDBObject(field + ".value", value))));
    }
}
