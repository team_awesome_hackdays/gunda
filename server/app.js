const express = require('express');
const session = require('express-session');
const path = require('path');
const uuidV4 = require('uuid/v4');
const zeromq = require('zeromq');
const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('./config');

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use((error, request, response, next) => {
    if (error !== null) {
        return response.json({ 'invalid': 'json' });
    }
    return next();
});

const httpServer = require('http').Server(app);
const requester = zeromq.socket('req');

const httpListenPort = config.httpListenPort;
const backendHost = config.backendHost;
const backendPort = config.backendPort;

var responses = {};

app.post('/save', function (req, res) {
    const transactionid = uuidV4();    
    // Add an UUID to keep the Resopnse
    req.body.transactionid = transactionid;      

    const request = JSON.stringify({
        ACTION: "SAVE",
        DATA: req.body
    });

    console.log("Save Request to backend: [ " + request.toString() + " ]");
    requester.send(request);
    res.send("OK");
    responses[transactionid] = res;
});

requester.on("message", function (reply) {
    console.log("Received reply from backend: [ " + reply.toString() + " ]");
    const message = JSON.parse(reply);
    const data = JSON.parse(message.DATA);
    const transactionid = data.transactionid;    
    const res = responses[transactionid];
    // do something with the response here...

    responses[transactionid] = null;    
    console.log(data);
});

app.use(express.static(__dirname));
app.use(express.static(__dirname + '/src'));

// Connect to backend
requester.connect("tcp://" + backendHost + ":" + backendPort);
console.log("Connecting to backend on tcp://" + backendHost + ":" + backendPort);

// Start this Webserver
httpServer.listen(httpListenPort, function () {
    console.log("Express server listening on 127.0.0.1:" + httpListenPort);
});
